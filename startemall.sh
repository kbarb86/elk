#!/bin/bash

#start elastic search
service elasticsearch start
service kibana start
service metricbeat start
service logstash start

tail -f /dev/null