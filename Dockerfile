FROM ubuntu:18.04
#CMD tail -f /dev/null
RUN apt-get update  && apt-get install -y wget gpg nano 
RUN wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch |  apt-key add -
RUN echo "deb https://artifacts.elastic.co/packages/6.x/apt stable main" | tee -a /etc/apt/sources.list.d/elastic-6.x.list
RUN apt-get install -y apt-transport-https pwgen uuid-runtime openjdk-8-jre-headless
RUN apt-get update
RUN apt-get install -y elasticsearch logstash kibana
COPY elasticsearch.yml /etc/elasticsearch/
COPY kibana.yml /etc/kibana/
RUN apt-get install -y metricbeat
COPY startemall.sh startemall.sh
ENTRYPOINT ["/startemall.sh"]